from django.conf.urls import *
from django.contrib import admin
from . import views
from django.conf import settings

urlpatterns = (
    url(r'^{}/'.format(settings.W_LOGIN), views.wlogin, name='wlogin'),
    url(r'^{}/$'.format(settings.W_LOGGED), views.wlogged, name='wlogged'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('social_django.urls')),
)

