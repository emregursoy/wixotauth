=====
Wixot Auth
=====

First install wAuth package to wixot-auth/dist/wixot-auth-0.1.tar.gz

Wixot Auth

Quick start
-----------
1.Settings::


2. Add "wAuth" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'wAuth',
    ]

3. Include the polls URLconf in your project urls.py like this::

    url(r'^',include('wAuth.urls')),

4. Run `python manage.py migrate` to create the polls models.

5. Start the development server and visit http://127.0.0.1:8000/wlogin/
   to auth with wixot mail.